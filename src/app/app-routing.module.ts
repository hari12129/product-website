import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { HomeComponent } from './home/home.component';
import { AppComponent } from './app.component';
const routes: Routes = [
  {path:'products',component:ProductsComponent},
  {path:'home',component:HomeComponent},
  {path:'app',component:AppComponent }

  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

