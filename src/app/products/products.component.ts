import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';

import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../app.component';



@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private router:Router){}

  onclick() : void{
  this.router.navigate(['/products'])
  }

 

  ngOnInit(): void {
  }

}
