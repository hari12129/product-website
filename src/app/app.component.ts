import { Component } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { RouterModule, Routes } from '@angular/router';
import { ActivatedRoute,Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task';
  constructor(private router:Router){}

  navigateToHome() {
    this.router.navigate(['home'])
  }
  navigateToProducts() {
    this.router.navigateByUrl('/products')
  }
}